package net.aliphaticus.gradlelocalremotedependencyexample;

import net.aliphaticus.wfilehandlerlib.WFilehandlerLib;
import org.slf4j.ILoggerFactory;
import org.slf4j.IMarkerFactory;
import org.slf4j.spi.MDCAdapter;
import org.slf4j.spi.SLF4JServiceProvider;

public class GradleLocalRemoteDependencyExample {

    public static void main(String[] args) {

        SLF4JServiceProvider slf4JServiceProvider = new SLF4JServiceProvider() {
            @Override
            public ILoggerFactory getLoggerFactory() {
                return null;
            }

            @Override
            public IMarkerFactory getMarkerFactory() {
                return null;
            }

            @Override
            public MDCAdapter getMDCAdapter() {
                return null;
            }

            @Override
            public String getRequestedApiVersion() {
                return null;
            }

            @Override
            public void initialize() {

            }
        };

        System.out.println(WFilehandlerLib.getDirectory());
        System.out.println("Hello World");
    }


}
